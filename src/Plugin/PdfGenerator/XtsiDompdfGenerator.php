<?php

/**
 * @file
 * Contains \Drupal\sxt_xtsipdf\Plugin\XtsiDompdfGenerator.
 */

namespace Drupal\sxt_xtsipdf\Plugin\PdfGenerator;

use Dompdf\Dompdf;
use Drupal\pdf_api\Plugin\PdfGenerator\DompdfGenerator;

/**
 * A PDF generator plugin for the dompdf library.
 *
 * @PdfGenerator(
 *   id = "xtsidompdf",
 *   module = "sxt_xtsipdf",
 *   title = @Translation("Xtsi DOMPDF"),
 *   description = @Translation("PDF generator using the DOMPDF generator.")
 * )
 */
class XtsiDompdfGenerator extends DompdfGenerator {
  /**
   * {@inheritdoc}
   */
//              public function __construct(array $configuration, $plugin_id, array $plugin_definition, DOMPDF $generator) {
//                parent::__construct($configuration, $plugin_id, $plugin_definition, $generator);
//
//                $this->generator->getOptions()->setIsPhpEnabled(TRUE);    // how to get working  ???
//              }

  /**
   * {@inheritdoc}
   */
  public function setter($pdf_content, $pdf_location, $save_pdf, $paper_orientation, $paper_size, $footer_content, $header_content, $path_to_binary = '') {
    $this->setPageOrientation($paper_orientation);
    $this->setPageSize($paper_size);

    $xx_paper_sizes = $this->generator->getPaperSize();


    $this->addPage($pdf_content);
    $this->setHeader($header_content);
    $this->setFooter($footer_content);
  }

  /**
   * {@inheritdoc}
   */
  public function setHeader($text) {
//    $canvas = $this->generator->get_canvas();
//    $canvas->page_text(48, 30, $text, "", 8, array(0, 0, 0));

//    public function line($x1, $y1, $x2, $y2, $color, $width, $style = array())
//    static function translate_attributes(Frame $frame)

        // This is where the magic happens:
//    Dompdf::render() -  $root->reflow(); // line 842

//            $content_x = $cb["x"] + $left;
//            $content_y = $cb["y"] + $top;
//            $content_width = $cb["w"] - $left - $right;
//            $content_height = $cb["h"] - $top - $bottom;



//    $canvas->page_text(48, 30, "<hr />", "", 10, array(0, 0, 0));
//    $canvas->page_text(48, 18, "Header: {PAGE_NUM}/{PAGE_COUNT}", "", 10, array(0, 0, 0));
//    $canvas->page_text(48, 18, "Header: {PAGE_NUM}/{PAGE_COUNT}", "", 10, array(0, 0, 0));
//    $canvas->page_text(72, 18, "Header: {PAGE_NUM}/{PAGE_COUNT}", "", 11, array(0, 0, 0));
//    $canvas->page_text(72, 18, $text, "", 11, array(0, 0, 0));
  }

  /**
   * {@inheritdoc}
   */
//  public function addPage($html) {
//    $xx_substr = substr($html, -2000);
//    parent::addPage($html);
//  }

  /**
   * {@inheritdoc}
   */
//  public function setPageOrientation($orientation = PdfGeneratorInterface::PORTRAIT) {
//    $this->generator->set_paper("", $orientation);
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  public function setPageSize($page_size) {
//    if ($this->isValidPageSize($page_size)) {
//      $this->generator->set_paper($page_size);
//    }
//  }

  /**
   * {@inheritdoc}
   */
  public function setFooter($text) {
    // @todo see issue over here: https://github.com/dompdf/dompdf/issues/571
    $paper_sizes = $this->generator->getPaperSize();
    $canvas = $this->generator->get_canvas();
//    $left = ($paper_sizes[2] / 2) - 20;
    $left = $paper_sizes[2] - 90;
    $top = $paper_sizes[3] - 48;
    $canvas->page_text(46, $top, "$text", "", 8, array(0, 0, 0));
    $canvas->page_text($left, $top, "- {PAGE_NUM} / {PAGE_COUNT} -", "", 8, array(0, 0, 0));
  }

}
