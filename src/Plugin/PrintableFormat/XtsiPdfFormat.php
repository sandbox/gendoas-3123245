<?php

/**
 * @file
 * Contains \Drupal\sxt_xtsipdf\Plugin\PrintableFormat\XtsiPdfFormat.
 */

namespace Drupal\sxt_xtsipdf\Plugin\PrintableFormat;

use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\printable_pdf\Plugin\PrintableFormat\PdfFormat;

/**
 * Provides a plugin to display a Xtsi PDF version of a page.
 *
 * @PrintableFormat(
 *   id = "xtsipdf",
 *   module = "sxt_xtsipdf",
 *   title = @Translation("Xtsi PDF"),
 *   description = @Translation("Xtsi PDF description.")
 * )
 */
class XtsiPdfFormat extends PdfFormat {
  
  protected $node_title = 'none';

  public function getHeaderContent() {
    return $this->node_title;
  }

  public function getFooterContent() {
    return $this->node_title;
  }

  /**
   * Get the HTML content for PDF generation.
   *
   * @return string
   *   HTML content for PDF.
   */
  public function buildPdfContent() {
    if (\Drupal::moduleHandler()->moduleExists('sxt_slogitem')) {
      $node = $this->content['#node'];
      $slogitem = XtsiNodeTypeData::nodeGetBaseSlogitem($node);
      if ($slogitem && $slogitem instanceof SlogItem) {
        $result = $slogitem->ajaxExecute();
        $this->node_title = $header = $node->label();
        $uri = str_replace('/index.php/', '/', \Drupal::request()->getUri());
        $footer = "$uri - $header";
        $clsheader = 'class="xtsipdf-header"';
        $clsfooter = 'class="xtsipdf-footer"';

        $build = [
          '#theme' => ['printable__xtsipdf', 'printable'],
          '#header' => [
            '#markup' => "<hr /><div $clsheader>$header</div><hr />",
          ],
          '#content' => [
            '#markup' => $result['content'],
          ],
          '#footer' => [
            '#markup' => "<hr /><span $clsfooter>$footer</span>",
          ],
        ];

        $rendered_page = parent::extractLinks(render($build));
        return $rendered_page;
      }
    }

    return parent::buildPdfContent();
  }

}
